package jamiejohnson.wordpuzzle;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;


public class MenuActivity extends AppCompatActivity {
    //fields
    private Button mBtnAbout;
    private Button mbtnNewGame;
    private RelativeLayout mRelativeLayout;
    private LinearLayout mLinearLayout;
    private TextView mTitleText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

        mRelativeLayout = (RelativeLayout)findViewById(R.id.relativeLayout);
        mLinearLayout =(LinearLayout)findViewById(R.id.linearLayout);
        mBtnAbout = (Button)findViewById(R.id.btnabout);
        mTitleText = (TextView)findViewById(R.id.textView);
        mbtnNewGame = (Button)findViewById(R.id.btnNewGame);

        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (v.getId()) {
                    case R.id.btnabout:
                        showAbout();
                        break;

                    case R.id.btnNewGame:
                        startNewGame();
                        break;
                }
            }
        };
        mBtnAbout.setOnClickListener(listener);
        mbtnNewGame.setOnClickListener(listener);

    }

    public void showAbout() {
        Intent intent = new Intent(MenuActivity.this, AboutActivity.class);
        startActivity(intent);
    }

    public void startNewGame() {
        Intent intent = new Intent(MenuActivity.this, GameActivity.class);
        startActivity(intent);
    }
}
