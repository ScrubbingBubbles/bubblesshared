package jamiejohnson.wordpuzzle;

import android.content.Context;
import android.content.res.AssetManager;
import android.renderscript.ScriptGroup;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.Scanner;

public class Dictionary {
    String word;
    Boolean aword;
    Context ctx;
    int score;

    public Dictionary(String word, Context ctx) {
        this.word = word;
        this.ctx = ctx;
        searchWord();
    }


    public int getScore(){
        this.score = calcScore();
        return this.score;
    }

    public boolean isaword(){
        return this.aword;
    }

    private void searchWord() {

        AssetManager mgr = ctx.getResources().getAssets();
        try {
            URL dictionary = (getClass().getResource("/assets/dictionary.txt"));
            BufferedReader br = new BufferedReader(new InputStreamReader(dictionary.openStream()));
            String line;
            {while ((line = br.readLine()) != null) {
                if(line.equalsIgnoreCase(word)) {
                    this.aword = true;
                    break;
                }
                else {
                    this.aword = false;
                }
            }

            }
        }
        catch (java.io.IOException e) {
            e.printStackTrace();
        }


    }

    private int calcScore() {
       return word.length();
    }


}
