package jamiejohnson.wordpuzzle;

import android.content.Context;
import android.graphics.Color;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Button;
import android.widget.Toast;

import java.util.concurrent.TimeUnit;
import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.Arrays;

public class GameActivity extends AppCompatActivity {
    private TextView mboard0;
    private TextView mboard1;
    private TextView mboard2;
    private TextView mboard3;
    private TextView mboard4;
    private TextView mboard5;
    private TextView mboard6;
    private TextView mboard7;
    private TextView mboard8;
    private TextView mboard9;
    private TextView mboard10;
    private TextView mboard11;
    private TextView mboard12;
    private TextView mboard13;
    private TextView mboard14;
    private TextView mboard15;
    private TextView mscore;
    private Button msubmitbutton;
    private TextView mcurrentword;
    private String[] gameboard;
    private View[][] boardViews;
    private View previousMove;
    private TextView mwrong1;
    private TextView mwrong2;
    private boolean previousMoveState;
    private String newWord;
    private StringBuilder str;
    private int score;
    private TextView mtimer;
    private CountDownTimer newtime;
    ArrayList<View> green;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);
        mboard0 = (TextView) findViewById(R.id.board0);
        mboard1 = (TextView) findViewById(R.id.board1);
        mboard2 = (TextView) findViewById(R.id.board2);
        mboard3 = (TextView) findViewById(R.id.board3);
        mboard4 = (TextView) findViewById(R.id.board4);
        mboard5 = (TextView) findViewById(R.id.board5);
        mboard6 = (TextView) findViewById(R.id.board6);
        mboard7 = (TextView) findViewById(R.id.board7);
        mboard8 = (TextView) findViewById(R.id.board8);
        mboard9 = (TextView) findViewById(R.id.board9);
        mboard10 = (TextView) findViewById(R.id.board10);
        mboard11 = (TextView) findViewById(R.id.board11);
        mboard12 = (TextView) findViewById(R.id.board12);
        mboard13 = (TextView) findViewById(R.id.board13);
        mboard14 = (TextView) findViewById(R.id.board14);
        mboard15 = (TextView) findViewById(R.id.board15);
        mscore = (TextView) findViewById(R.id.textscore);
        msubmitbutton = (Button) findViewById(R.id.submitbutton);
        mcurrentword = (TextView) findViewById(R.id.currentword);
        mwrong1 = (TextView) findViewById(R.id.wrong1);
        mwrong2 = (TextView) findViewById(R.id.wrong2);
        mtimer = (TextView) findViewById(R.id.timetext);

        boardViews = new View[][]{{mboard0, mboard1, mboard2, mboard3},
                {mboard4, mboard5, mboard6, mboard7},
                {mboard8, mboard9, mboard10, mboard11},
                {mboard12, mboard13, mboard14, mboard15}};

        CountDownTimer newtime = new CountDownTimer(120000, 100) {
            @Override
            public void onTick(long millisUntilFinished) {
                mtimer.setText(String.format("%d:%02d",
                        TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished),
                        TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) -
                                TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished))
                ));

            }

            @Override
            public void onFinish() {
                Toast newtoast = Toast.makeText(GameActivity.this, "Game Over!", Toast.LENGTH_LONG);
                newtoast.show();
            }
        }.start();

        Board board = new Board();
        gameboard = board.getBoard();
        this.setBoard(gameboard);
        this.previousMoveState = false;
        score = 0;
        mscore.setText("Score: " + score);
        green = new ArrayList<View>();


        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TextView clicked = (TextView) v;
                if (!previousMoveState) {
                    makeMove(clicked);
                } else {
                    clicked.setTag(2);
                    Move newMove = new Move(boardViews, previousMove, clicked);
                    if (newMove.getLegal()) {
                        previousMove.setTag(0);
                        makeMove(clicked);

                    } else {
                        clicked.setTag(0);
                    }
                }
            }
        };
        mboard0.setOnClickListener(listener);
        mboard1.setOnClickListener(listener);
        mboard2.setOnClickListener(listener);
        mboard3.setOnClickListener(listener);
        mboard4.setOnClickListener(listener);
        mboard5.setOnClickListener(listener);
        mboard6.setOnClickListener(listener);
        mboard7.setOnClickListener(listener);
        mboard8.setOnClickListener(listener);
        mboard9.setOnClickListener(listener);
        mboard10.setOnClickListener(listener);
        mboard11.setOnClickListener(listener);
        mboard12.setOnClickListener(listener);
        mboard13.setOnClickListener(listener);
        mboard14.setOnClickListener(listener);
        mboard15.setOnClickListener(listener);


        View.OnClickListener sublistener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Context ctx = getApplicationContext();
                Dictionary mydic = new Dictionary(newWord, ctx);
                if (mydic.isaword()) {
                    score = score + (mydic.getScore());
                    mscore.setText("Score: " + score);
                    boolean goodword = true;
                    reset(goodword);
                }
                else {
                    boolean goodword = false;
                    reset(goodword);
                }

            }
        };
        msubmitbutton.setOnClickListener(sublistener);
    }


    private void setBoard(String[] gameboard) {
        gameboard = gameboard;
        mboard0.setText(gameboard[0]);
        mboard1.setText(gameboard[1]);
        mboard2.setText(gameboard[2]);
        mboard3.setText(gameboard[3]);
        mboard4.setText(gameboard[4]);
        mboard5.setText(gameboard[5]);
        mboard6.setText(gameboard[6]);
        mboard7.setText(gameboard[7]);
        mboard8.setText(gameboard[8]);
        mboard9.setText(gameboard[9]);
        mboard10.setText(gameboard[10]);
        mboard11.setText(gameboard[11]);
        mboard12.setText(gameboard[12]);
        mboard13.setText(gameboard[13]);
        mboard14.setText(gameboard[14]);
        mboard15.setText(gameboard[15]);

    }

    private void makeMove(View v) {
        TextView clicked = (TextView) v;
        clicked.setBackgroundColor(Color.parseColor("#9DED9A"));
        if (!previousMoveState) {
            StringBuilder str = new StringBuilder();
            str.append(clicked.getText().toString());
            newWord = str.toString();
            clearWrong();
        } else {
            StringBuilder str = new StringBuilder(newWord);
            str.append(clicked.getText().toString());
            newWord = str.toString();
        }
        green.add(clicked);
        mcurrentword.setText(newWord);
        previousMoveState = true;
        previousMove = clicked;
        previousMove.setTag(1);
    }

    private void reset(Boolean goodword) {
        previousMoveState = false;
        previousMove.setTag(0);
        if (!goodword) {
            setWrong();
        }
        newWord = "";
        mcurrentword.setText(newWord);
        for (int i = 0; i < (green.size()); i++) {
            green.get(i).setBackgroundColor((Color.parseColor("#FFFDE8")));
        }
    }

    private void setWrong() {
        mwrong1.setText("X");
        mwrong2.setText("X");

    }
    private void clearWrong() {
        mwrong1.setText("");
        mwrong2.setText("");
    }


}




