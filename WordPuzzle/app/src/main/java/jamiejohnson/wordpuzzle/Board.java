package jamiejohnson.wordpuzzle;

import java.util.Arrays;
import java.util.Random;

public class Board {
    private String LETTERS="abcdefghijklmnopqrstuvwxyzabcdefghilmnoprstaeiou";
    private String[] gameboard;

    public Board() {
        this.LETTERS = LETTERS;
        this.gameboard = this.chooseLetters();
        checkQ();
    }

    public String[] getBoard() {
        return this.gameboard;
    }

    private String[] chooseLetters() {
        StringBuilder candidates = new StringBuilder(this.LETTERS);
        StringBuilder chosenLetters = new StringBuilder();
        Random r = new Random();
        for (int i = 0; i < 16; i++) {
            int index = r.nextInt(candidates.length());
            chosenLetters.append(candidates.charAt(index));
            candidates.deleteCharAt(index);
        }
        return chosenLetters.toString().split("(?<!^)");


    }

    private void checkQ() {
        for (int i = 0; i < 16; i++)
            if (Arrays.asList(this.gameboard).indexOf("q") == i) {
                this.gameboard[i] = "qu";
                break;
            }
    }




}
