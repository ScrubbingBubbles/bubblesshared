package jamiejohnson.wordpuzzle;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.View;

public class Move {
    View previousMove;
    View currentMove;
    View[][] boardviews;
    int[] currentCoord;
    int[] previousCoord;
    boolean legal;

    public Move(View[][] boardviews, View previousMove, View currentMove) {
        this.boardviews = boardviews;
        this.previousMove = previousMove;
        this.currentMove = currentMove;
        findCurrentLocation();
        findLastLocation();
    }

    public boolean getLegal() {
        checkLegal();
        return legal;
    }

    private void checkLegal() {
            if (((ColorDrawable)currentMove.getBackground()).getColor() ==
                Color.parseColor("#9DED9A"))
            {
                this.legal = false;
            }
            else this.legal = (Math.abs(this.previousCoord[0] - this.currentCoord[0]) <= 1) &&
                    Math.abs(this.previousCoord[1] - this.currentCoord[1]) <= 1;

    }

    private void findCurrentLocation() {
        for (int i = 0; i <4; i++) {
            for (int j = 0; j < 4; j++) {
                if (currentMove.getTag() == boardviews[i][j].getTag())
                {
                    this.currentCoord = new int[] {i, j};

                }
            }
        }
    }

    private void findLastLocation() {
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
                if (previousMove.getTag() == boardviews[i][j].getTag())
                {
                    this.previousCoord = new int[]{i, j};
                }
            }
        }
    }
}
