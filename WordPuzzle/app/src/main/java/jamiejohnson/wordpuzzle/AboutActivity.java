package jamiejohnson.wordpuzzle;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.content.Intent;


public class AboutActivity extends AppCompatActivity {
    private Button mbtnOK;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);

        mbtnOK = (Button)findViewById(R.id.btnOK);

        View.OnClickListener oklistener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AboutActivity.this, MenuActivity.class);
                startActivity(intent);

            }
        };
        mbtnOK.setOnClickListener(oklistener);




    }
}
